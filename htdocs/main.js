/*
    photomap, a Leaflet map of geotagged photos.
    Copyright (C) 2015 by Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Define some parameters.  */
initialPos = [48, 15];
initialZoom = 5;
thumbSize = 240;

/* ************************************************************************** */
/* Main class wrapping the map and all functionalities we want.  */

/* Construct the object, which sets up a base map with tiles but
   without any additional features yet.  */
function PhotoMap (containerId)
{
  var properties =
    {
      center: initialPos,
      minZoom: 0,
      zoom: initialZoom
    };
  this.map = L.map ("map", properties);

  attr = '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors';
  properties =
    {
      attribution: attr,
      subdomains: ["a", "b", "c"]
    };
  var tiles = L.tileLayer ("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                           properties);
  tiles.addTo (this.map);
}

/* Add (clustered) markers for the given photo data with the given
   marker icon.  */
PhotoMap.prototype.addPhotos = function (icon, photos)
{
  if (this.photoLayer !== undefined)
    throw "already have photo layer";

  this.photoLayer = L.markerClusterGroup ({showCoverageOnHover: false});
  for (var i = 0; i < photos.length; ++i)
    {
      var cur = photos[i];
      if (cur === null)
        continue;

      var coord = L.latLng (cur.latitude, cur.longitude);
      properties =
        {
          icon: icon,
          title: cur.date
        };
      var marker = L.marker (coord, properties);

      var w, h;
      if (cur.width > cur.height)
        {
          w = thumbSize;
          h = cur.height * thumbSize / cur.width;
        }
      else
        {
          w = cur.width * thumbSize / cur.height;
          h = thumbSize;
        }
      var style = "width: " + w + "px; height: " + h + "px;";

      var popup = '<a href="' + cur.file + '" target="_blank">"';
      popup += '<img src="' + cur.file + '" title="' + cur.date + '"';
      popup += ' style="' + style + '" /></a>';
      marker.bindPopup (popup);

      this.photoLayer.addLayer (marker);
    }
  this.photoLayer.addTo (this.map);
}

/* Set list of countries to highlight on the map.  The countries to actually
   show are given as a list of two-letter ISO codes.  All country borders
   themselves are encoded as GeoJSON (per the data file).  */
PhotoMap.prototype.addCountries = function (borders, list)
{
  if (this.countryLayer !== undefined)
    throw "already have countries layer";

  /* Change list to dictionary, so we can quickly look up which
     countries to show.  */
  var countries = {};
  for (var i = 0; i < list.length; ++i)
    countries[list[i]] = true;

  function filter (feature, layer)
    {
      var code = feature.properties.ISO2;
      return countries[code];
    }

  this.countryLayer = L.geoJson (borders, {filter: filter});
  this.countryLayer.addTo (this.map);
}

/* Add layer control to the map.  */
PhotoMap.prototype.addLayerControl = function ()
{
  var overlayMaps = {};
  if (this.photoLayer !== undefined)
    overlayMaps["Photos"] = this.photoLayer;
  if (this.countryLayer !== undefined)
    overlayMaps["Visited Countries"] = this.countryLayer;

  L.control.layers (null, overlayMaps).addTo (this.map);
}

/* ************************************************************************** */

function init ()
{
  var map = new PhotoMap ("map");
  map.addCountries (borders, countries);

  var properties =
    {
      iconUrl: "heart.png",
      iconSize: [24, 24],
      iconAnchor: [12, 12],
      popupAnchor: [0, -15]
    };
  var icon = L.icon (properties);
  map.addPhotos (icon, photos);

  map.addLayerControl ();
}

window.addEventListener ("load", init, false);
