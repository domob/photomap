#!/usr/bin/python

#   photomap, a Leaflet map of geotagged photos.
#   Copyright (C) 2015 by Daniel Kraft <d@domob.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Process a list of geotagged image files into a JSON data
# file for use by the map.

import json
import re
import subprocess
import sys

import shapely.geometry

digs = "(\\d+)";
szPattern = "Image Size +: %sx%s.*\\n" % (digs, digs)
datePattern = "Date/Time Original +: %s:%s:%s.*\\n" % (digs, digs, digs);
degPattern = "%s deg %s' ([0-9.]+)\"" % (digs, digs)
gpsPattern = "GPS Position +: %s ([NS]), %s ([WE])" % (degPattern, degPattern)
expr = re.compile (szPattern + datePattern + gpsPattern)

def extractDeg (g):
  """
  Take a match group for degPattern plus NS/WE specification and
  turn it into a signed floating-point number representing the
  single degree value.
  """

  (deg, minute, second, spec) = g
  value = int (deg) + int (minute) / 60.0 + float (second) / 3600.0

  if spec in ["N", "E"]:
    return value
  if spec in ["S", "W"]:
    return -value

  sys.stderr.write ("invalid NS/WE specification found\n")
  sys.exit (-1)

def processFile (fn):
  """
  Process a single file and return a JSON data object for it.
  """

  res = {"file": fn}

  exif = subprocess.check_output (["exiftool", "-ImageSize",
                                   "-DateTimeOriginal", "-GPSPosition", fn])
  match = expr.match (exif)
  if match is None:
    sys.stderr.write ("error processing file %s\n" % fn)
    return None

  (width, height) = map (int, match.group (1, 2))
  res["width"] = width
  res["height"] = height

  (year, month, day) = map (int, match.group (3, 4, 5))
  res["date"] = "%02d. %02d. %04d" % (day, month, year)

  res["latitude"] = extractDeg (match.group (6, 7, 8, 9))
  res["longitude"] = extractDeg (match.group (10, 11, 12, 13))

  return res

def findCountry (borders, lat, lng):
  """
  Find the country (if any) in the borders GeoJSON file that contains
  the given lat/lng coordinate.  Returned is the country's two-letter
  ISO code.
  """

  # Note that GeoJSON stores the coordinates as lng/lat, so we have
  # to make the point in the same format.  Otherwise, Shapely won't
  # be able to correctly relate both objects.
  pt = shapely.geometry.Point (lng, lat)

  for f in borders["features"]:
    poly = shapely.geometry.shape (f["geometry"])
    if poly.contains (pt):
      return f["properties"]["ISO2"]

  return None

################################################################################

args = sys.argv[1:]
if len (args) < 1:
  sys.stderr.write ("USAGE: processFiles.py BORDERS [PHOTO PHOTO ...]\n")
  sys.exit (-1)

borderFile = args[0]
photos = args[1:]

sys.stderr.write ("loading border file...\n")
with open (borderFile, "r") as f:
  borders = json.load (f)

sys.stderr.write ("extracting picture information...\n")
data = map (processFile, photos)
print ("var photos = %s;" % json.dumps (data))

sys.stderr.write ("finding visited countries...\n")
countries = {}
for d in data:
  if d is not None:
    c = findCountry (borders, d["latitude"], d["longitude"])
    if c is not None:
      countries[c] = True
print ("var countries = %s;" % json.dumps (countries.keys ()))
